---
title: Title project 
author: Facundo Muñoz
date: "`r format(Sys.Date(), '%b, %Y')`"
output:
  pdf_document: default
  html_document: default
documentclass: cirad
---



## Context

### 2021-08-24 M Bordier (e-mail communication)

Sends the FAO-OIE document `doc/PPR-Global-Strategy-avecAnnexes_2015-03-28.pdf` _Global Strategy for the control and eradication of PPR._ (2015) and asks a few questions about the _post-vaccination evaluation tool_ in Annex 3.4 (p. 179).


### 2021-05-19 M Bordier (e-mail communication)

M Bordier invites L Manso Silvan and myself to participate in the Cirad's team for the following collaboration.

CICR: Comité International de la Croix Rouge

- Scientific and technical support to the CICR's PPR and PPCB vaccination activities on three topics:
    - development of surveillance protocols, 
    - development of vaccination protocols 
    - interpretation of serology results for the evaluation of vaccination campaigns.
    
- 2 or 3 one-week workshops between the end of 2021 and the end of 2022 with CICR correspondents in the various countries concerned by the vaccination programmes (Mauritania, Mali, Burkina Faso, Niger, Cameroon, Chad, Central African Republic, Nigeria)

- To provide a theoretical basis on these three themes, which they could then apply to their fields with our support.

- The workshops would take place in one of the capitals of the sub-region (probably Dakar where the CICR has its regional delegation). No field mission.
- Team: Adama Diallo (PPR), Marion Bordier (Epidemiology), Facundo Muñoz (Biostatistics), Lucía Manso-Silvan (PPCB)




