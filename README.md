
<!-- README.md is generated from README.Rmd. Please edit that file -->

# CICR Vaccination

<!-- badges: start -->
<!-- badges: end -->

Collaboration project with the Red-Cross International Comitee (CICR)
for providing scientific and technical support to PPR and PPCB
vaccination activities on three topics: - development of surveillance
protocols, - development of vaccination protocols - interpretation of
serology results for the evaluation of vaccination campaigns.

## Documents

-   [Statistical methods of the post-vaccionation evaluation
    tool](https://umr-astre.pages.mia.inra.fr/cicr-vaccination/statistics_pvet.html)
